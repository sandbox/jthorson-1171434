<?php

/**
 * @file
 * A module providing a IP Address entity and related bundles
 *
 * This module provides an IP Address entity, and various subtypes representing
 * a single IP address, a simple range of IP addresses, a CIDR range, or an IP
 * network address with netmask.
 */

/**
 * Implements hook_help().
 */
function ipaddr_help($path, $arg) {
  if ($path == 'admin/help#ipaddr') {
    return t('Module which defines an IP Address entity and related bundles.');
  }
}

/**
 * Implements hook_permission().
 */
function ipaddr_permission() {
  $permissions = array();
  $permissions['administer ipaddrs'] = array(
    'title' => t('Administer the IP Address entity'),
  );
  $permissions['create ipaddrs'] = array(
    'title' => t('Create IP Address entity instances')
  );
  $permissions['view ipaddrs'] = array(
    'title' => t('View IP Address entity instances')
  );
  $permissions['update ipaddrs'] = array(
    'title' => t('Update IP Address entity instances')
  );
  $permissions['delete ipaddrs'] = array(
    'title' => t('Delete IP Address entity instances')
  );
  return $permissions;
}

/**
 * Implements hook_menu().
 */
function ipaddr_menu() {
  $items['admin/structure/ipaddr'] = array(
    'title' => 'Manage IP Address entities',
    'description' => 'Manage IP Address entities, including addition/removal of types and management of associated fields.',
    'page callback' => 'ipaddr_overview_types',
    'access arguments' => array('administer ipaddrs'),
    'file' => 'ipaddr.admin.inc',
  );
  $items['admin/structure/ipaddr/view'] = array(
    'title' => 'List Types',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/structure/ipaddr/add'] = array(
    'title' => 'Add Type',
    'description' => 'Add IPAddr Type',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ipaddr_type_form'),
    'access arguments' => array('administer ipaddrs'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
    'file' => 'ipaddr.admin.inc',
  );
  $items['admin/structure/ipaddr/manage/%ipaddr_type'] = array(
    'title callback' => 'ipaddr_type_page_title',
    'title arguments' => array(4),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ipaddr_type_form', 4),
    'access arguments' => array('administer ipaddrs'),
    'file' => 'ipaddr.admin.inc',
  );
  $items['admin/structure/ipaddr/manage/%ipaddr_type/edit'] = array(
    'title' => 'Edit Type',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/structure/ipaddr/manage/%ipaddr_type/list'] = array(
    'title' => 'List Types',
    'page callback' => 'drupal_goto',
    'page arguments' => array('admin/structure/ipaddr/manage'),
    'access arguments' => array('administer ipaddrs'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -5,
  );
  $items['admin/structure/ipaddr/manage/%ipaddr_type/delete'] = array(
    'title' => t('Delete'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ipaddr_deletetype_confirm', 4),
    'access arguments' => array('delete ipaddrs'),
    'weight' => 10,
    'type' => MENU_LOCAL_TASK,
    'file' => 'ipaddr.admin.inc',
  );
  $items['ipaddr/add'] = array(
    'title' => 'Add new IP Address',
    'page callback' => 'ipaddr_page_add',
    'access arguments' => array('create ipaddrs'),
    'weight' => 1,
    'menu_name' => 'management',
    'file' => 'ipaddr.pages.inc',
  );
  foreach (ipaddr_types() as $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $items['ipaddr/add/' . $type_url_str] = array(
      'title' => $type->name,
      'page callback' => 'ipaddr_add',
      'page arguments' => array(2),
      'access arguments' => array('create ipaddrs'),
      'description' => $type->description,
      'file' => 'ipaddr.pages.inc',
    );
    $items['ipaddr/list/' . $type_url_str] = array(
      'title' => $type->name,
      'page callback' => 'ipaddr_page_list',
      'page arguments' => array(2),
      'access arguments' => array('view ipaddrs'),
      'file' => 'ipaddr.pages.inc',
    );
  }
  $items['ipaddr/%ipaddr'] = array(
    'title callback' => 'ipaddr_page_title',
    'title arguments' => array(1),
    'page callback' => 'ipaddr_page_view',
    'page arguments' => array(1),
    'access arguments' => array('view ipaddrs'),
    'type' => MENU_VISIBLE_IN_BREADCRUMB,
    'file' => 'ipaddr.pages.inc',
  );
  $items['ipaddr/%ipaddr/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['ipaddr/%ipaddr/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'ipaddr_page_edit',
    'page arguments' => array(1),
    'access arguments' => array('update ipaddrs'),
    'weight' => 0,
    'type' => MENU_LOCAL_TASK,
    'file' => 'ipaddr.pages.inc',
  );
  $items['ipaddr/%ipaddr/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ipaddr_delete_confirm', 1),
    'access arguments' => array('delete ipaddrs'),
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
    'file' => 'ipaddr.pages.inc',
  );
  $items['ipaddr'] = array(
    'title' => 'List IP Addresses',
    'page callback' => 'ipaddr_page_list',
    'page arguments' => array('all'),
    'access arguments' => array('view ipaddrs'),
    'file' => 'ipaddr.pages.inc',
  );
  $items['ipaddr/default'] = array(
    'title' => '"Default" address types',
    'page callback' => 'ipaddr_page_list',
    'page arguments' => array('default'),
    'access arguments' => array('view ipaddrs'),
    'file' => 'ipaddr.pages.inc',
    'weight' => -5
  );
  $items['ipaddr/custom'] = array(
    'title' => '"Custom" address types',
    'page callback' => 'ipaddr_page_list',
    'page arguments' => array('custom'),
    'access arguments' => array('view ipaddrs'),
    'file' => 'ipaddr.pages.inc',
    'weight' => -3,
  );
  return $items;
}

/**
 * Implements hook_entity_info().
 */
function ipaddr_entity_info() {
  $items['ipaddr'] = array(
    'label' => t('IP Address'),
    'controller class' => 'IPAddrController',
    'base table' => 'ipaddr',
    'revision table' => 'ipaddr_revision',
    'uri callback' => 'ipaddr_uri',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'ipid',
      'revision' => 'vid',
      'bundle' => 'type',
      'label' => 'label',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'static cache' => TRUE,
    'bundles' => array(

    ),
    'view modes' => array(
      'full' => array(
        'label' => t('Full address with label'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Teaser'),
        'custom settings' => FALSE,
      ),
    ),
  );

  foreach (ipaddr_types() as $type => $info) {
    $items['ipaddr']['bundles'][$type] = array(
      'label' => $info->name,
      'admin' => array(
        'path' => 'admin/structure/ipaddr/manage/%ipaddr_type',
        'real path' => 'admin/structure/ipaddr/manage/' . str_replace('_', '-', $type),
        'bundle argument' => 4,
        'access arguments' => array('administer ipaddrs'),
      ),
    );
  }

  return $items;
}

/**
 * Returns a list of IPAddr entity types (ie. bundles)
 */
function ipaddr_types() {
  $types = &drupal_static(__FUNCTION__);
  if (empty($types)) {
    $sql = "Select id, type, name, description from {ipaddr_type}";
    $types = db_query($sql)->fetchAllAssoc('type');
    foreach ($types as $key => $info) {
      $types[$key]->type = filter_xss($info->type);
      $types[$key]->name = filter_xss($info->name);
      $types[$key]->description = filter_xss($info->description);
    }
  }
  return $types;
}

/**
 * Loads a ipaddr_type
 *
 * Used as a menu placeholder loader, but may also be leveraged by other functions.
 *
 * @param $type A IPAddr entity type to load
 *
 * @return $type Loaded IPAddr entity type, or FALSE if none found.
 */
function ipaddr_type_load($type) {
  $types = ipaddr_types();
  $type = str_replace('-', '_', $type);
  return isset($types[$type]) ? $types[$type] : FALSE;
}

/**
 * Page Title callback for IPAddr types
 *
 * @param $type
 * The machine-readable type (ie. bundle) of the IP Addr entity
 */
function ipaddr_type_page_title($type) {
  return t('"@type" IP Address Type', array('@type' => $type->name));
}

/**
 * Returns the humanreadable name for an IP Address type
 */
function ipaddr_type_name($ipaddrtype) {
  $result = db_query("Select name from {ipaddr_type} WHERE type = :type", array(':type' => $ipaddrtype))->fetchField();
  return filter_xss($result);
}

/**
 * Returns an initialized ipaddr object.
 *
 * @param $type
 * The machine-readable type of the ipaddr.
 *
 * @return
 * An ipaddr object with all default fields initialized.
 */
function ipaddr_new($type = '') {
  return entity_get_controller('ipaddr')->create($type);
}

/**
 * Load an IPAddr object from the database.
 *
 * @param $ipid
 * The IPAddr ID.
 * @param $vid
 * The revision ID.
 * @param $reset
 * Whether to reset the ipaddr_load_multiple cache.
 *
 * @return
 * A fully-populated IPAddr object.
 */
function ipaddr_load($ipid = NULL, $vid = NULL, $reset = FALSE) {
  $ipids = (isset($ipid) ? array($ipid) : array());
  $conditions = (isset($vid) ? array('vid' => $vid) : array());
  $ipaddr = ipaddr_load_multiple($ipids, $conditions, $reset);
  return $ipaddr ? reset($ipaddr) : FALSE;
}

/**
 * Load IPAddr entities from the database.
 *
 * This function should be used whenever you need to load more than one IPAddr
 * from the database. IPAddrs are loaded into memory and will not require
 * database access if loaded again during the same page request.
 *
 * @see entity_load()
 *
 * @param $aids
 * An array of ipaddr IDs.
 * @param $conditions
 * An array of conditions on the {ipaddr} table in the form 'field' => $value.
 * @param $reset
 * Whether to reset the internal entity_load cache.
 *
 * @return
 * An array of IPAddr objects indexed by nid.
 */
function ipaddr_load_multiple($ipids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('ipaddr', $ipids, $conditions, $reset);
}

/**
 * Save an IPAddr object.
 *
 * @param $ipaddr
 * The IPAddr to be saved.
 * @return
 * The saved ipaddr, now with an ipid if necessary.
 */
function ipaddr_save($ipaddr) {
  return entity_get_controller('ipaddr')->save($ipaddr);
}

/**
 * Deletes an ipaddr by ID.
 *
 * @param $ipid
 * The ID of the IPAddr to delete.
 *
 * @return
 * TRUE on success, FALSE otherwise.
 */
function ipaddr_delete($ipid) {
  return ipaddr_delete_multiple(array($ipid));
}

/**
 * Deletes multiple ipaddrs by ID.
 *
 * @param $ipids
 * An array of ipaddr IDs to delete.
 *
 * @return
 * TRUE on success, FALSE otherwise.
 */
function ipaddr_delete_multiple($ipids) {
  return entity_get_controller('ipaddr')->delete($ipids);
}

/**
 * URI_Callback function for IPAddr entities.
 */
function ipaddr_uri($ipaddr) {
  return array(
    'path' => 'ipaddr/' . $ipaddr->ipid,
  );
}

/**
 * Sets the page title based on the specified ipaddr.
 */
function ipaddr_page_title($ipaddr) {
  return ipaddr_build_title($ipaddr);
}

/**
 * Builds a title for an IPAddr instance
 */
function ipaddr_build_title($ipaddr) {
  $type = ipaddr_type_name($ipaddr->type);
  $ip = long2ip($ipaddr->ipaddr_1);
  switch ($ipaddr->type) {
    case 'simple_ip':
      $label = $type . ': ' . $ip;
      break;
    case 'simple_range':
      $label = $type . ': ' . $ip . ' - ' . long2ip($ipaddr->ipaddr_2);
      break;
    case 'cidr_range':
      $label = $type . ': ' . $ip . '/' . $ipaddr->cidr;
      break;
    case 'network_with_mask':
      $label = $type . ': ' . $ip . ' (' . long2ip($ipaddr->ipaddr_2) . ')';
      break;
    default:
      $label = $type . ': ' . $ip . ' ' . long2ip($ipaddr->ipaddr_2) . ' ' . $ipaddr->cidr;
      break;
  }
  return $label;
}

/**
 * Implements hook_field_extra_fields().
 */
function ipaddr_field_extra_fields() {
  $extra['ipaddr'] = array(
    'simple_ip' => array(
      'form' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('IP Address'),
          'description' => t('The standalone IP Address for this entry.'),
          'weight' => -10,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -9,
        ),
      ),
      'display' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('IP Address'),
          'description' => t('The standalone IP Address for this entry.'),
          'weight' => -10,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -9,
        ),
      ),
    ),
    'simple_range' => array(
      'form' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('First IP Address'),
          'description' => t('The first IP Address of the range.'),
          'weight' => -10,
        ),
        'ipaddr_2' => array(
          'label' => t('Last IP Address'),
          'description' => t('The last IP Address of the range.'),
          'weight' => -8,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -7,
        ),
      ),
      'display' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('First IP Address'),
          'description' => t('The first IP Address of the range.'),
          'weight' => -10,
        ),
        'ipaddr_2' => array(
          'label' => t('Last IP Address'),
          'description' => t('The last IP Address of the range.'),
          'weight' => -8,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -7,
        ),
      ),
    ),
    'cidr_range' => array(
      'form' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('CIDR Network Address'),
          'description' => t('The CIDR Network address for the subnet.'),
          'weight' => -10,
        ),
        'cidr' => array(
          'label' => t('CIDR Mask'),
          'description' => t('The CIDR network mask for the subnet.'),
          'weight' => -8,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -7,
        ),
      ),
      'display' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('CIDR Network Address'),
          'description' => t('The CIDR Network address for the subnet.'),
          'weight' => -10,
        ),
        'cidr' => array(
          'label' => t('CIDR Mask'),
          'description' => t('The CIDR network mask for the subnet.'),
          'weight' => -8,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -7,
        ),
      ),
    ),
    'network_with_mask' => array(
      'form' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('Network Address'),
          'description' => t('The network address for the subnet.'),
          'weight' => -10,
        ),
        'ipaddr_2' => array(
          'label' => t('Subnet Mask'),
          'description' => t('The network mask for the subnet.'),
          'weight' => -8,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -7,
        ),
      ),
      'display' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('The human readable label for this entry'),
          'weight' => -11,
        ),
        'ipaddr_1' => array(
          'label' => t('Network Address'),
          'description' => t('The network address for the subnet.'),
          'weight' => -10,
        ),
        'ipaddr_2' => array(
          'label' => t('Subnet Mask'),
          'description' => t('The network mask for the subnet.'),
          'weight' => -8,
        ),
        'comment' => array(
          'label' => t('Comments'),
          'description' => t('Optional user comments associated with this entry'),
          'weight' => -7,
        ),
      ),
    ),
  );
  return $extra;
}

/**
 * Implements hook_forms().
 */
function ipaddr_forms() {
  $forms = array();
  if ($types = ipaddr_types()) {
    foreach (array_keys($types) as $type) {
      $forms[$type . '_ipaddr_form']['callback'] = 'ipaddr_form';
    }
  }
  return $forms;
}



/**
 * Implements hook_theme()
 */
function ipaddr_theme($existing, $type, $theme, $path) {
  return array(
    'ipaddr' => array(
      'variables' => array(
        'ipaddr' => array(),
        'link' => FALSE,
      ),
    ),
    'ipaddr_table' => array(
      'variables' => array(
        'ipaddrs' => array(),
        'links' => FALSE,
        'headers' => FALSE,
      ),
    ),
    'ipaddr_table_row' => array(
      'variables' => array(
        'ipaddr' => NULL,
        'links' => FALSE,
        'headers' => FALSE,
      ),
    ),
  );
}

/**
 * Themes the IP Address Listing Table
 */
function theme_ipaddr_table($variables) {
  $ipaddrs = $variables['ipaddrs'];
  // Include links to the entity nodes
  $links = $variables['links'];
  // Format ranges as headers
  $headers = $variables['headers'];
  $header = array(
    array('data' => t('IP Listing'), 'class' => 'simpletest_test'),
    array('data' => t('Comment')),
    array('data' => t('Type')),
  );

  foreach($ipaddrs as $ipaddr) {
    $rows[] = theme('ipaddr_table_row', array('ipaddr' => $ipaddr, 'links' => $links, 'headers' => $headers, ));
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Themes a row of the IP Address Listing Table
 */
function theme_ipaddr_table_row($variables) {
  $ipaddr = $variables['ipaddr'];
  // Include links to entity node page
  $link = $variables['links'];
  // Format ranges as headers
  $headers = $variables['headers'];
  $row = array(
    'data' => array(
      array(
        'data' => theme('ipaddr', array('ipaddr' => $ipaddr, 'link' => $link)),
        'class' => 'ipaddr_table_address',
      ),
      array(
          'data' => !empty($ipaddr->comment) ? filter_xss($ipaddr->comment) : '',
          'class' => 'ipaddr_table_addresscomment',
      ),
      array(
        'data' => ipaddr_type_name($ipaddr->type),
        'class' => 'ipaddr_table_addresstype',
      ),
    )
  );
  if ($headers && in_array($ipaddr->type, array('simple_range', 'cidr_range', 'network_with_mask'))) {
    // Range defined ... make it a header
    $row['data'][0]['header'] = 'header';
    $row['data'][1]['header'] = 'header';
    $row['data'][2]['header'] = 'header';
    $row['class'] = array('ipaddr_tablerow_range');
  }
  elseif ($ipaddr->type == 'simple_ip') {
    $row['class'] = array('ipaddr_tablerow_address');
  }
  else {
    $row['class'] = array('ipaddr_tablerow_custom');
  }
  return $row;
}

/**
 * Themes an IP Address entity instance into a human-readable label
 */
function theme_ipaddr($variables) {
  $ipaddr = $variables['ipaddr'];
  $link = $variables['link'];
  $type = ipaddr_type_name($ipaddr->type);
  $ip = long2ip($ipaddr->ipaddr_1);
  switch ($ipaddr->type) {
    case 'simple_ip':
      $label = $ip;
      break;
    case 'simple_range':
      $label = $ip . ' - ' . long2ip($ipaddr->ipaddr_2);
      break;
    case 'cidr_range':
      $label = $ip . '/' . $ipaddr->cidr;
      break;
    case 'network_with_mask':
      $label = $ip . ' (' . long2ip($ipaddr->ipaddr_2) . ')';
      break;
    default:
      $label = $ip . ', (' . long2ip($ipaddr->ipaddr_2) . '),  /' . $ipaddr->cidr;
      break;
  }
  if ($link) {
    $label = l($label, 'ipaddr/' . $ipaddr->ipid);
  }
  return $label;
}

/**
 * Retrieves IP Address Entities
 */
function ipaddr_retrieve_ipaddrs($type = 'all') {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'ipaddr');
  if ($type == "default") {
    // Retrieve IP Address instances of all 'default' IP Address Types
    $query->propertyCondition('type', array('simple_ip', 'simple_range', 'cidr_range', 'network_with_mask'));
  }
  elseif ($type == "custom") {
    // Retrieve IP Address instances of all 'custom' IP Address Types
    $query->propertyCondition('type', array('simple_ip', 'simple_range', 'cidr_range', 'network_with_mask'), "NOT IN");
  }
  elseif ($type != "all") {
    // Retrieve IP Address instances of a specific IP Address Type
    $query->propertyCondition('type', $type);
  }
  $result = $query->propertyOrderBy('ipaddr_1', 'ASC')
    ->propertyOrderBy('ipaddr_2', 'DESC')
    ->pager(130)
    ->execute();
  return $result;
}

/**
 * IP Address Validator
 */
function ipaddr_valid_ipv4addr($addr) {
  if (preg_match('/^((1?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(1?\d{1,2}|2[0-4]\d|25[0-5])$/', $addr)) {
    return true;
  }
  return false;
}

/**
 * IP Mask Validator
 */
function ipaddr_valid_ipv4netmask($mask){
  $format = '';

  if (ipaddr_valid_ipv4addr($mask)) {
    $format = "long";
  }
  elseif (($mask > 0) && ($mask <= 32)) {
    $format = "short";
  }
  else {
    return FALSE;
  }
  switch($format){
    case 'long';
      if (!($mask = ipaddr_ip2long($mask))) {
        return FALSE;
      }
      $mask = decbin($mask);
      break;
    case 'short':
      $tmp = $mask;
      for ($i = 0; $i < $mask; $i++){
        $tmp.= 1;
      }
      for ($j = 0; $j < (32 - $mask); $j++){
        $tmp.= 0;
      }
      $mask = $tmp;
      break;
  }
  $mask = str_pad($mask, 32, "0", STR_PAD_LEFT);
  if (strlen($mask) <= 32) {
    for ($i = 0; $i <= 32 ; $i++){
      $bit = substr($mask, $i ,1);
      if (($bit - substr($mask,$i+1,1)) < 0) {
        return false;
      }
    }
  }
  return true;
}

/**
 * Convert IP Address to unsigned int
 *
 * Used to perform comparison functions (Not currently used?)
 */
function ipaddr_tounsignedint($ip) {
  return substr($ip, 0, 3) > 127 ? ((ipaddr_ip2long($ip) & 0x7FFFFFFF) + 0x80000000) : ipaddr_ip2long($ip);
}

/**
 * Wrapper for the ip2long function, ensuring result as 32-bit signed int
 */
function ipaddr_ip2long32($ip) {
  list(, $ip) = unpack('l', pack('l', ip2long($ip)));
  return $ip;
}

/**
 * Wrapper for the ip2long function, ensuring result as an unsigned int (or possibly, a float 'double' on 32-bit machines)
 */
function ipaddr_ip2long($ip) {
  // Check whether platform can natively support ip2long('255.255.255.255') as an integer
  if (PHP_INT_MAX > 4294967294) {
    $result = ip2long($ip);
  }
  // If not, we need to ensure we generate an 'unsigned' value (which may be int or double/float)
  elseif (substr($ip, 0, 3) > 127) {
    $result = (ip2long($ip) & 0x7FFFFFFF) + 0x80000000;
  }
  else {
    $result = ip2long($ip);
  }
  return $result;
}

/**
 * Convert CIDR Mask to Subnet Netmask
 */
function ipaddr_cidr2netmask($cidr) {
  return ipaddr_ip2long(long2ip(ipaddr_ip2long("255.255.255.255") << (32 - $cidr)));
}
