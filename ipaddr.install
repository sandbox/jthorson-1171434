<?php

/**
 * @file
 *
 * Installation functions for the IPv4 Address Entity module
 */

/**
 * Implementation of hook_schema().
 *
 * Generates three tables:
 *   - ipaddr:  Stores the data associated with an individual IPAddr entity instance
 *   - ipaddr_revisions:  Stores revision information for an IPAddr entity instance
 *   - ipaddr_type:  Stores information about IPAddr Entity bundles (ie. 'types')
 */
function ipaddr_schema() {
  $schema['ipaddr'] = array(
    'description' => 'The base table for IPAddr Entities.',
    'fields' => array(
      'ipid' => array(
        'description' => 'The Unique ID of the IP Address',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => 'The current {ipaddr_revision}.vid version identifier.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'type' => array(
        'description' => 'The {ipaddr_type} of this entry.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'ipaddr_1' => array(
        'description' => 'The IP Address (for single IP or a simple range), or network address for this entry.',
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'precision' => 10,
        'scale' => 0,
        'default' => '0',
      ),
      'ipaddr_2' => array(
        'description' => 'The ending IP Address (for simple range), or network mask for this entry.',
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'precision' => 10,
        'scale' => 0,
        'default' => 0,
      ),
      'cidr' => array(
        'description' => 'The CIDR Mask for this entry.',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => '32',
      ),
      'label' => array(
        'description' => 'A human readable label for this entry, used to reference the entity instance.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'comment' => array(
        'description' => 'An optional user comment to be associated with this entry.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the ipaddr was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the ipaddr was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'unique keys' => array(
      'ipid_vid' => array('ipid', 'vid'),
      'ipid' => array('ipid'),
    ),
    'primary key' => array('ipid'),
  );
  $schema['ipaddr_revision'] = array(
    'description' => 'Stores information about each saved version of an {ipaddr}.',
    'fields' => array(
      'ipid' => array(
        'description' => 'The {ipaddr} this version belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'vid' => array(
        'description' => 'The primary identifier for this version.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'ipaddr_1' => array(
        'description' => 'The IP Address (for single IP or a simple range), or network address for this version.',
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => '0',
        'precision' => 10,
        'scale' => 0,
      ),
      'ipaddr_2' => array(
        'description' => 'The ending IP Address (for simple range), or network mask for this version.',
        'type' => 'numeric',
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
        'precision' => 10,
        'scale' => 0,
      ),
      'cidr' => array(
        'description' => 'The CIDR Mask for this version.',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => '32',
      ),
      'comment' => array(
        'description' => 'An optional user comment to be associated with this entry.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the ipaddr version was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'ipid' => array('ipid'),
    ),
    'primary key' => array('vid'),
    'foreign keys' => array(
      'ipaddr' => array(
        'table' => 'ipaddr',
        'columns' => array(
          'ipid' => 'ipid',
        ),
      ),
    ),
  );
  $schema['ipaddr_type'] = array(
    'description' => 'Stores information about IPAddr types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'The primary numeric identifier for this IPAddr type.',
      ),
      'type' => array(
        'description' => 'The unique machine-name for this IPAddr type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'name' => array(
        'description' => 'The human-readable identifier for this IPAddr type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A description for this IPAddr type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'unique keys' => array(
      'id_type' => array('id', 'type'),
      'id' => array('id'),
      'type' => array('type'),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}

/**
 * Implements hook_install().
 *
 * Inserts the default IPv4 Address Entity bundle values into the database
 */
function ipaddr_install() {
  // Insert default values
  $query = db_insert('ipaddr_type')
    ->fields(array('type', 'name', 'description'))
    ->values(array('type' => 'simple_ip', 'name' => 'Simple IP', 'description' => 'A single standalone IPv4 address'))
    ->values(array('type' => 'simple_range', 'name' => 'Simple Range', 'description' => 'A range of IPv4 addresses, defined by the first and last addresses of the range'))
    ->values(array('type' => 'cidr_range', 'name' => 'CIDR Range', 'description' => 'An IPv4 network subnet, defined by the network address and CIDR mask.'))
    ->values(array('type' => 'network_with_mask', 'name' => 'Network with Mask', 'description' => 'An IPv4 network subnet, defined by the network address and subnet mask.'))
    ->execute();
}

