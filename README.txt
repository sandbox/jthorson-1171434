The IP Address Entity module introduces an "IP Address Entity" on your site,
supporting the following IP Address Types (ie. bundles):

    Simple IP Address
    Simple IP Address Range
        (defined via the 'start' and 'end' addresses of the range)
    CIDR IP Subnet
        (defined via a 'network' address and numeric CIDR mask)
    Network with Netmask
        (defined via a 'network' address and IP Netmask)

The module provides an admin interface at admin/structure/ipaddrs for the
management of these IP Address types (bundles), as well as the definition
of new 'custom' types.

It also supports full add/edit/delete/view functionality for individual
instances of these IP Address types, and the use of fields to extend
IP Address types.
