<?php
/**
 * @file
 *
 * IPv4 Address Entity module administration page callbacks and functions
 */

/**
 * Menu call back for the admin/structure/ipaddr page
 *
 * Provides a list all IPAddr types, and related edit, delete, and fields
 * related management links.
 */
function ipaddr_overview_types() {
  // Build an administration table
  $header = array(t('IP Address Type'), t('Machine name'), t('Description'), array('data' => t('Operations'), 'colspan' => '4'));
  foreach(ipaddr_types() as $type => $info) {
    $type_url_str = str_replace('_', '-', check_plain($type));
    // Note: Info output sanitized during ipaddr_types() call
    $row = array($info->name, $info->type, $info->description);
    $row[] = l(t('Edit'), 'admin/structure/ipaddr/manage/' . $type_url_str . '/edit');
    $row[] = l(t('Manage Fields'), 'admin/structure/ipaddr/manage/' . $type_url_str. '/fields');
    $row[] = l(t('Manage Display'), 'admin/structure/ipaddr/manage/' . $type_url_str. '/display');
    if (!(in_array($type, array('simple_ip', 'simple_range', 'cidr_range', 'network_with_mask')))) {
      $row[] = l(t('Delete'), 'admin/structure/ipaddr/manage/' . $type_url_str . '/delete');
    }
    else {
      $row[] = '';
    }
    $rows[] = $row;
  }
  if (isset($rows)) {
  	$build = array();
  	$build['table'] = array(
  	  '#theme' => 'table',
  	  '#header' => $header,
  	  '#rows' => $rows,
  	);
  	$build['pager'] = array(
  	  '#theme' => 'pager',
  	);
    return $build;
  }
  else {
  	$message = '<br />';
  	$message .= t('No IP Address types exist on the system.');
  	$message .= '<br /><br />';
  	$message .= t('Please create an IP Address type which can be assigned to addresses.');
  	return $message;
 }
}

/**
 * Form Declaration for ipaddr_type_form
 *
 * Provides a form which can be used to define new IPAddr types (ie. bundles)
 */
function ipaddr_type_form($form, &$form_state, $ipaddr_type = NULL) {
  // Set the id to identify this as an IPAddr edit form.
  $form['#id'] = 'ipaddr-addtype-form';

  // Save the ipaddr_type for later, in case we need it.
  $form['#ipaddr_type'] = $ipaddr_type;
  $form_state['ipaddr_type'] = $ipaddr_type;

  // Retrieve the existing types
  $types = ipaddr_types();

  // Render existing IP Address types with the form
  if (!empty($types)) {
    $header = array(t('Id'), t('Type'), t('Name'), t('Description'));

    $form['existingtype'] = array(
      '#markup' => theme('table', $header, $types),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }
  else {
    $message = '<br />';
    $message .= t('No IP Address types exist on the system.');
    $message .= '<br /><br />';
    $message .= t('Please create an IP Address type which can be assigned to addresses.');
    $form['existingtype'] = array(
      '#markup' => $message,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('IP Address Type'),
    '#description' => t('The human-readable identifier for this IPAddr type.'),
    '#default_value' => isset($ipaddr_type->name) ? filter_xss($ipaddr_type->name) : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['type'] = array(
    //'#type' => 'textfield',
    '#type' => 'machine_name',
    '#title' => t('Type'),
    '#description' => t('The unique machine-name for this IPAddr type.'),
    '#default_value' => isset($ipaddr_type->type) ? check_plain($ipaddr_type->type) : '',
    '#maxlength' => 32,
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'ipaddr_type_load',
      'source' => array('name'),
    ),
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('A description for this IPAddr type.'),
    '#default_value' => isset($ipaddr_type->description) ? filter_xss($ipaddr_type->description) : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#weight' => 40,
  );
  if (!isset($ipaddr_type)) {
    $form['actions']['submit']['#value'] = t('Save IP Address Type');
  }
  else {
    $form['actions']['submit']['#value'] = t('Update IP Address Type');
  }
  return $form;
}

/**
 * Submission function for the ipaddr_type_form form.
 *
 * Creates a new IPAddr type (ie. bundle).
 */
function ipaddr_type_form_submit(&$form, &$form_state) {
  if (isset($form_state['ipaddr_type']->id)) {
    // Update existing entry
    $query = db_update('ipaddr_type');
    $query->condition('id', $form_state['ipaddr_type']->id);
  }
  else {
    // Insert new entry
    $query = db_insert('ipaddr_type');
  }
  $query->fields(array(
      'type' => $form_state['values']['type'],
      'name' => $form_state['values']['name'],
      'description' => $form_state['values']['description'],
    ))
    ->execute();
  $form_state['redirect'] = 'admin/structure/ipaddr';
  drupal_set_message(t('IP Address Type saved.'));
}


/**
 * Confirm form for the 'Delete IPAddr type' action
 *
 * Prompts the user with confirmation before deleting an IPAddr type (bundle)
 */
function ipaddr_deletetype_confirm($form, &$form_state, $ipaddr_type) {
  $form['#ipaddr_type'] = $ipaddr_type;
  $form_state['ipaddr_type'] = $ipaddr_type;

  $question = t('Are you sure you want to delete the IP Address type %name?', array('%name' => $ipaddr_type->name));
  $message = t('All IP Address entity instances of this type will also be deleted from your site.');
  $message .= '<br />';
  $message .= t('This action cannot be undone.');

  // Always provide entity id in the same form key as in the entity edit form.
  $form['id'] = array('#type' => 'value', '#value' => $ipaddr_type->id);
  return confirm_form($form,
    $question,
    'admin/structure/ipaddr/manage',
    $message,
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submission routine for the 'Delete IPAddr Type' confirmation form
 *
 * Deletes the IPAddr type (ie. bundle)
 */
function ipaddr_deletetype_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $ipaddr_type = $form_state['ipaddr_type'];
    // Delete IP Address Type
    $query = db_delete('ipaddr_type')
      ->condition('id', $ipaddr_type->id)
      ->execute();
    // Retrieve all Instances of this IP Address type
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'ipaddr')
      ->propertyCondition('type', $ipaddr_type->type)
      ->execute();
    $ipids = array_keys($result['ipaddr']);
    // Delete all instances of this IP Address type
    ipaddr_delete_multiple($ipids);
    $count = count($result['ipaddr']);

    drupal_set_message(t('IP Address type %name has been deleted. (!count instance(s) of this type also deleted.)', array('%name' => $ipaddr_type->name, '!count' => $count)));
    watchdog('ipaddr', t('IP Address type %name has been deleted. (!count instance(s) of this type also deleted.)', array('%name' => $ipaddr_type->name, '!count' => $count)));
  }
  $form_state['redirect'] = 'admin/structure/ipaddr/manage';
}
