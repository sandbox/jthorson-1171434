<?php
/**
 * @file
 * Contains page callbacks related to the IPAddr entity module
 */

/**
 * Displays an ipaddr.
 *
 * @param $ipaddr
 * The ipaddr object to display.
 * @param $view_mode
 * The view mode we want to display.
 */
function ipaddr_page_view($ipaddr, $view_mode = 'full') {
  // Title will be displayed automatically on full page view
  if ($view_mode == 'full') {
    if (!isset($ipaddr->content)) {
      $ipaddr->content = array();
      if (!empty($ipaddr->label)) {
        $ipaddr->content['label'] = array(
          '#markup' => t('Label') . ': ' . check_plain($ipaddr->label),
        );
      }
      if (!empty($ipaddr->comment)) {
        $ipaddr->content['label'] = array(
          '#markup' => t('Comment') . ': ' . check_plain($ipaddr->comment),
        );
      }
    }
  }
  if ($view_mode == 'teaser') {
    $ipaddr->content['title'] = array(
      '#markup' => ipaddr_page_title($ipaddr),
      '#weight' => -5,
    );
  }

  // Build fields content.
  field_attach_prepare_view('ipaddr', array($ipaddr->ipid => $ipaddr), $view_mode);
  entity_prepare_view('ipaddr', array($ipaddr->ipid => $ipaddr));
  $ipaddr->content += field_attach_view('ipaddr', $ipaddr, $view_mode);

  return $ipaddr->content;
}

/**
 * Menu callback; presents the ipaddr editing form, or redirects to delete confirmation.
 *
 * @param $ipaddr
 * The ipaddr object to edit.
 */
function ipaddr_page_edit($ipaddr) {
  $types = ipaddr_types();
  // Type sanitized in ipaddr_types(), Title still requires.
  drupal_set_title(t('<em>Edit !type</em> @title', array('!type' => $types[$ipaddr->type]->name, '@title' => $ipaddr->label)), PASS_THROUGH);
  return drupal_get_form($ipaddr->type . '_ipaddr_form', $ipaddr);
}

/**
 * Renders the IP Address listing
 */
function ipaddr_page_list($type = 'all') {
  // Initialize content array
  $content = array();

  // Perform -/_ substitution, since passed a url string
  $type = str_replace('-', '_', check_plain($type));
  $result = ipaddr_retrieve_ipaddrs($type);
  if (isset($result['ipaddr'])) {
    $ipaddrs = ipaddr_load_multiple(array_keys($result['ipaddr']));
    $content['ipaddr_table'] = array(
      '#theme' => 'ipaddr_table',
      '#ipaddrs' => $ipaddrs,
      '#links' => TRUE,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
    if (in_array($type, array('all', 'default'))) {
      $content['ipaddr_table']['#headers'] = TRUE;
    }
    $content['pager'] = array(
      '#theme' => 'pager',
      '#weight' => 5,
    );
  }
  else {
    $content['ipaddr_table'] = array(
      '#markup' => t('No matching IP Address instances exist on this site.'),
    );
  }

  return $content;
}


/**
 * Generates a list of 'add' instance links, one for each of the various IPAddr types
 */
function ipaddr_page_add() {
  $item = menu_get_item();
  $links = system_admin_menu_block($item);

  foreach ($links as $link) {
    $items[] = l($link['title'], $link['href'], $item['localized_options']) . ': ' . filter_xss_admin($link['description']);
  }
  if (isset($items)) {
    return theme('item_list', array('items' => $items));
  }
  else {
    $message = '<br />';
    $message .= t('No IP Address Types found.');
    $message .= '<br /><br />';
    $message .= t('Please create at least one "IP Address Type", so that it may be assigned to this address entry.');
    return $message;
  }
}

/**
 * Menu callback for adding a new IPAddr instance
 */
function ipaddr_add($type) {
  global $user;
  $types = ipaddr_types();
  $type = isset($type) ? str_replace('-', '_', check_plain($type)) : NULL;
  if (empty($types[$type])) {
    return MENU_NOT_FOUND;
  }
  $ipaddr = entity_get_controller('ipaddr')->create($type);
  drupal_set_title(t('Create instance of IP Address type "!name"', array('!name' => $types[$type]->name)), PASS_THROUGH);
  return drupal_get_form($type . '_ipaddr_form', $ipaddr);
}

/**
 * Implements the 'add IPAddr' form
 */
function ipaddr_form($form, &$form_state, $ipaddr) {
  // Set the id to identify this as an IPAddr edit form.
  $form['#id'] = 'ipaddr-form';

  // Save the ipaddr for later, in case we need it.
  $form['#ipaddr'] = $ipaddr;
  $form_state['ipaddr'] = $ipaddr;

  $form['ipaddr_1'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($ipaddr->ipaddr_1) ? long2ip($ipaddr->ipaddr_1) : '',
    '#weight' => -10,
    '#required' => TRUE,
  );

  switch ($ipaddr->type) {
    case 'simple_ip':
      $form['ipaddr_1']['#title'] = t('IP Address');
      $form['ipaddr_1']['#description'] = t('The standalone IP Address for this entry.');
      break;
    case 'simple_range':
      $form['ipaddr_1']['#title'] = t('First IP Address');
      $form['ipaddr_1']['#description'] = t('The first IP Address of the range.');
      $form['ipaddr_2'] = array(
        '#type' => 'textfield',
        '#title' => t('Last IP Address'),
        '#default_value' => isset($ipaddr->ipaddr_2) ? long2ip($ipaddr->ipaddr_2) : '',
        '#description' => t('The last IP Address of the range.'),
        '#weight' => -8,
      );
      break;
    case 'cidr_range':
      $form['ipaddr_1']['#title'] = t('Network Address');
      $form['ipaddr_1']['#description'] = t('The network address for the CIDR range.');
      $form['cidr'] = array(
        '#type' => 'textfield',
        '#title' => t('CIDR Netmask'),
        '#default_value' => isset($ipaddr->cidr) ? check_plain($ipaddr->cidr) : '',
        '#description' => t('The CIDR Netmask for this entry (for a /24, enter "24").'),
        '#weight' => -8,
      );
      break;
    case 'network_with_mask':
      $form['ipaddr_1']['#title'] = t('Network Address');
      $form['ipaddr_1']['#description'] = t('The network address for this entry.');
      $form['ipaddr_2'] = array(
        '#type' => 'textfield',
        '#title' => t('Subnet Mask'),
        '#default_value' => isset($ipaddr->ipaddr_2) ? long2ip($ipaddr->ipaddr_2) : '',
        '#description' => t('The Network mask for the IP Subnet.'),
        '#weight' => -8,
      );
      break;
    default:
      $form['ipaddr_1']['#title'] = t('IP Address');
      $form['ipaddr_1']['#description'] = t('Required: An IP (or network) address for this entry');
      $form['ipaddr_2'] = array(
        '#type' => 'textfield',
        '#title' => t('Secondary IP Address'),
        '#default_value' => isset($ipaddr->ipaddr_2) ? long2ip($ipaddr->ipaddr_2) : '',
        '#description' => t('Optional: This field may contain an IP address or network mask, in "xxx.xxx.xxx.xxx" format.'),
        '#weight' => -8,
      );
      $form['cidr'] = array(
        '#type' => 'textfield',
        '#title' => t('CIDR Mask'),
        '#default_value' => isset($ipaddr->cidr) ? check_plain($ipaddr->cidr) : '',
        '#description' => t('Optional: This field may contain a CIDR mask (number from 1 to 32).'),
        '#weight' => -6,
      );
  }

  // Common fields.
  $form['comment'] = array(
    '#type' => 'textfield',
    '#title' => t('Comment'),
    '#description' => t('Optional:  Add a user comment which will be associated with this address entry'),
    '#default_value' => isset($ipaddr->comment) ? filter_xss($ipaddr->comment) : '',
    '#weight' => 5,
    '#required' => FALSE,
  );

  // Add the buttons
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('ipaddr_form_submit'),
  );
  if (!empty($ipaddr->ipid)) {
    $form['revision'] = array(
      '#access' => user_access('administer ipaddrs'),
      '#type' => 'checkbox',
      '#title' => t('Create new revision'),
      '#default_value' => 0,
      '#weight' => 9,
    );
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete ipaddrs'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('ipaddr_form_delete_submit'),
    );
  }
  // Add the 'create multiple' option checkbox to ranges if adding for first time
  elseif (in_array($ipaddr->type, array('simple_range', 'cidr_range', 'network_with_mask'))) {
    $form['createmultiple'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create individual IPs'),
      '#description' => t('Create a new "Simple IP Address" entry for each address in the range'),
      '#default_value' => FALSE,
      '#weight' => 8,
    );
  }
  $form['#validate'][] = 'ipaddr_form_validate';

  field_attach_form('ipaddr', $ipaddr, $form, $form_state);

  return $form;
}

/**
 * Validation callback for ipaddr_form
 */
function ipaddr_form_validate($form, &$form_state) {
  $ipaddr = $form_state['ipaddr'];
  // Field validation
  field_attach_form_validate('ipaddr', $ipaddr, $form, $form_state);

  switch ($ipaddr->type) {
    case 'simple_ip':
      // Simple IP Validation
      if (!ipaddr_valid_ipv4addr($form_state['values']['ipaddr_1'])) {
      	form_set_error('ipaddr_1', t('Please enter a valid IP Address'));
      }
      break;
    case 'simple_range':
      // Simple Range Validation
      if (!ipaddr_valid_ipv4addr($form_state['values']['ipaddr_1'])) {
      	form_set_error('ipaddr_1', t('Please enter a valid IP Address'));
      }
      if (!ipaddr_valid_ipv4addr($form_state['values']['ipaddr_2'])) {
      	form_set_error('ipaddr_2', t('Please enter a valid IP Address'));
      }
      if (ipaddr_ip2long($form_state['values']['ipaddr_1']) > ipaddr_ip2long($form_state['values']['ipaddr_2'])) {
      	form_set_error('ipaddr_2', t('Invalid Range:  Second IP address must be higher than the first.'));
      }
      break;
    case 'cidr_range':
      // CIDR Range Validation
      if (!ipaddr_valid_ipv4addr($form_state['values']['ipaddr_1'])) {
     	form_set_error('ipaddr_1', t('Invalid Network Address'));
      }
      if ((!ipaddr_valid_ipv4netmask($form_state['values']['cidr'])) || $form_state['values']['cidr'] < 1) {
    	form_set_error('cidr', t('Invalid CIDR Netmask'));
      }
      // Check that ip submitted is a valid network address
      // Get Netmask
      $netmask = (ipaddr_ip2long("255.255.255.255") << (32 - $form_state['values']['cidr']));
      $network = (ipaddr_ip2long($form_state['values']['ipaddr_1']) & $netmask);
      if ($form_state['values']['ipaddr_1'] != long2ip($network)) {
        form_set_error('ipaddr_1', t('Invalid Network Address for the given CIDR mask.  (Do you mean !ip?)', array('!ip' => long2ip($network))));
      }
      if (isset($form_state['values']['createmultiple'])
        && ($form_state['values']['createmultiple'])
        && ($form_state['values']['cidr'] < 20)) {
        form_set_error('createmultiple', t('The "create multiple" option supports a maximum of 4,096 addresses (minimum CIDR mask of 20).'));
      }
      break;
    case 'network_with_mask':
      // Network with mask validation
      if (!ipaddr_valid_ipv4addr($form_state['values']['ipaddr_1'])) {
    	form_set_error('ipaddr_1', t('Invalid Network Address'));
      }
      if (!ipaddr_valid_ipv4netmask($form_state['values']['ipaddr_2'])) {
     	form_set_error('ipaddr_2', t('Invalid Subnet Netmask'));
      }
      // Check that ip submitted is a valid network address
      $network = (ipaddr_ip2long($form_state['values']['ipaddr_1']) & (ipaddr_ip2long($form_state['values']['ipaddr_2'])));
      if ($form_state['values']['ipaddr_1'] != long2ip($network)) {
        form_set_error('ipaddr_1', t('Invalid Network Address for the given Netmask.  (Do you mean !ip?)', array('!ip' => long2ip($network))));
      }
      if (isset($form_state['values']['createmultiple'])
        && ($form_state['values']['createmultiple'])
        && (ipaddr_ip2long($form_state['values']['ipaddr_2']) < 4294963200)) {
        form_set_error('createmultiple', t('The "create multiple" option supports a maximum of 4,096 addresses (minimum subnet mask of 255.255.240.0).'));
      }
      break;
    default:
  }
}

/**
 * Submit callback for ipaddr_form
 */
function ipaddr_form_submit($form, &$form_state) {

  global $user;

  $ipaddr = &$form_state['ipaddr'];

  // Set the ipaddr's uid if it's being created at this time.
  if (empty($ipaddr->uid)) {
    $ipaddr->uid = $user->uid;
  }
  if (isset($form_state['values']['ipaddr_1'])) {
    $ipaddr->ipaddr_1 = ipaddr_ip2long($form_state['values']['ipaddr_1']);
  }
  if (isset($form_state['values']['ipaddr_2'])) {
    $ipaddr->ipaddr_2 = ipaddr_ip2long($form_state['values']['ipaddr_2']);
  }
  if (isset($form_state['values']['cidr'])) {
    $ipaddr->cidr = $form_state['values']['cidr'];
    if ($ipaddr->type == 'cidr_range') {
      $ipaddr->ipaddr_2 = ipaddr_cidr2netmask($ipaddr->cidr);
    }
  }
  if (isset($form_state['values']['comment'])) {
    $ipaddr->comment = $form_state['values']['comment'];
  }
  if (isset($form_state['values']['revision'])) {
    $ipaddr->revision = $form_state['values']['revision'];
  }
  $ipaddr->label = theme('ipaddr', array('ipaddr' => $ipaddr));

  if (isset($form_state['values']['createmultiple'])
    && ($form_state['values']['createmultiple'])
    && (isset($ipaddr->ipaddr_2) || isset($ipaddr->cidr))) {

    ipaddr_form_createmultiple($ipaddr, $form, $form_state);
  }

  // Notify field widgets.
  field_attach_submit('ipaddr', $ipaddr, $form, $form_state);

  // Save the ipaddr.
  ipaddr_save($ipaddr);

  // Redirect to the newly created node
  $form_state['redirect'] = 'ipaddr/' . $ipaddr->ipid;
}



/**
 * Creates multipe IP Address entries based on the 'Add IPAddr' form submission
 */
function ipaddr_form_createmultiple($ipaddr, $form, &$form_state) {

  // Determine type
  $type = $ipaddr->type;

  // Determine start, end, broadcast, and network ips
  switch ($type) {
    case 'simple_range':
      $start = $ipaddr->ipaddr_1;
      $end = $ipaddr->ipaddr_2;
      break;
    case 'cidr_range':
    case 'network_with_mask':
      // Determine network address
      $network = bindec(decbin($ipaddr->ipaddr_1) & decbin($ipaddr->ipaddr_2));
      // Determine first 'simple' address
      $start = $network + 1;
      // Determine broadcast address
      // Check for 255.255.255.255 case
      if ($ipaddr->ipaddr_2 == PHP_INT_MAX) {
        $end = ipaddr_ip2long("255.255.255.254");
        $broadcast = $end + 1;
      }
      else {
        // Calculation gives us a signed integer.
        // Therefore, we convert it over and back again.
        $broadcast = ipaddr_ip2long(long2ip($network | (~$ipaddr->ipaddr_2)));
        $end = $broadcast - 1;
      }
      break;
  }
  $count = 0;
  for ($i = $start; $i <= $end; $i++) {
    $ip = ipaddr_new('simple_ip');
    $ip->uid = $ipaddr->uid;
    $ip->ipaddr_1 = $i;
    if (isset($ipaddr->comment)) {
      $ip->comment = $ipaddr->comment;
    }
    $ip->label = theme('ipaddr', array('ipaddr' => $ip));

    // Notify field widgets.
    field_attach_submit('ipaddr', $ip, $form, $form_state);

    // Save the ipaddr.
    ipaddr_save($ip);
    $count++;
  }

  // Notify the user.
  drupal_set_message(t('@count IP Addresses created.', array('@count' => ($count))));

}


/**
 *  Form callback to delete an IPAddr
 */
function ipaddr_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $ipaddr = $form['#ipaddr'];
  $form_state['redirect'] = array('ipaddr/' . $ipaddr->ipid . '/delete', array('query' => $destination));
}

/**
 * Form bulder; Asks for confirmation of ipaddr deletion.
 */
function ipaddr_delete_confirm($form, &$form_state, $ipaddr) {
  $form['#ipaddr'] = $ipaddr;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['ipid'] = array('#type' => 'value', '#value' => $ipaddr->ipid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => theme('ipaddr', array('ipaddr' => $ipaddr)))),
    'ipaddr/' . $ipaddr->ipid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes ipaddr deletion.
 */
function ipaddr_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $ipaddr = ipaddr_load($form_state['values']['ipid']);
    ipaddr_delete($form_state['values']['ipid']);
    watchdog('ipaddr', '@type: deleted %title.', array('@type' => $ipaddr->type, '%title' => $ipaddr->label));

    $types = ipaddr_types();
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $types[$ipaddr->type]->name, '%title' => $ipaddr->label)));
  }

  $form_state['redirect'] = '<front>';
}

