<?php

/**
 * @file
 *
 * Contains the 'IPAddr Entity' controller class
 */

/**
 * Controller for loading, creating, and saving IPAddrs.
 *
 * The default loader, which we extend, handles load() already. We only
 * need to add saving and creating.
 */
class IPAddrController extends DrupalDefaultEntityController {

  /**
   * Create a default IPAddr.
   *
   * @param $type
   * The machine-readable type of the ipaddr.
   *
   * @return
   * An ipaddr object with all default fields initialized.
   */
  public function create($type = '') {
    return (object) array(
      'ipid' => '',
      'type' => $type,
      'label' => '',
      'comment' => '',
    );
  }

  /**
   * Save a IPAddr.
   *
   * @param $ipaddr
   * The ipaddr object to save.
   *
   * @return
   * The saved ipaddr object, or FALSE if an error encountered
   */
  public function save($ipaddr) {
    $transaction = db_transaction();

    try {
      global $user;

      // Determine if we will be inserting a new ipaddr.
      $ipaddr->is_new = empty($ipaddr->ipid);

      // Set the timestamp fields.
      if (empty($ipaddr->created)) {
        $ipaddr->created = REQUEST_TIME;
      }
      $ipaddr->changed = REQUEST_TIME;

      $ipaddr->revision_timestamp = REQUEST_TIME;
      $update_ipaddr = TRUE;

      // Give modules the opportunity to prepare field data for saving.
      field_attach_presave('ipaddr', $ipaddr);

      // When saving a new ipaddr revision, unset any existing $ipaddr->vid
      // to ensure a new revision will actually be created and store the old
      // revision ID in a separate property for ipaddr hook implementations.
      if (!$ipaddr->is_new && !empty($ipaddr->revision) && $ipaddr->vid) {
        $ipaddr->old_vid = $ipaddr->vid;
        unset($ipaddr->vid);
      }

      // If this is a new ipaddr...
      if ($ipaddr->is_new) {
        // Save the new ipaddr.
        drupal_write_record('ipaddr', $ipaddr);

        // Save the initial revision.
        $this->saveRevision($ipaddr, $user->uid);

        $op = 'insert';
      }
      else {
        // Save the updated ipaddr.
        drupal_write_record('ipaddr', $ipaddr, 'ipid');

        // If a new ipaddr revision was requested, save a new record for that;
        // otherwise, update the ipaddr revision record that matches the value
        // of $ipaddr->vid.
        if (!empty($ipaddr->revision)) {
          $this->saveRevision($ipaddr, $user->uid);
        }
        else {
          $this->saveRevision($ipaddr, $user->uid, TRUE);
          $update_ipaddr = FALSE;
        }

        $op = 'update';
      }

      // If the revision ID is new or updated, save it to the ipaddr.
      if ($update_ipaddr) {
        db_update('ipaddr')
          ->fields(array('vid' => $ipaddr->vid))
          ->condition('ipid', $ipaddr->ipid)
          ->execute();
      }

      // Save fields.
      $function = 'field_attach_' . $op;
      $function('ipaddr', $ipaddr);

      module_invoke_all('entity_' . $op, $ipaddr, 'ipaddr');

      // Clear internal properties.
      unset($ipaddr->is_new);

      // Ignore slave server temporarily to give time for the saved order to be
      // propagated to the slave.
      db_ignore_slave();

      return $ipaddr;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('ipaddr', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Saves an ipaddr revision with the uid of the current user.
   *
   * @param $ipaddr
   * The fully loaded ipaddr object.
   * @param $uid
   * The user's uid for the current revision.
   * @param $update
   * TRUE or FALSE indicating whether or not the existing revision should be
   * updated instead of a new one created.
   */
  function saveRevision($ipaddr, $uid, $update = FALSE) {
    // Hold on to the ipaddr's original creator_uid but swap in the revision's
    // creator_uid for the momentary write.
    $temp_uid = $ipaddr->uid;
    $ipaddr->uid = $uid;

    // Update the existing revision if specified.
    if ($update) {
      drupal_write_record('ipaddr_revision', $ipaddr, 'vid');
    }
    else {
      // Otherwise insert a new revision. This will automatically update $ipaddr
      // to include the vid.
      drupal_write_record('ipaddr_revision', $ipaddr);
    }

    // Reset the order's creator_uid to the original value.
    $ipaddr->uid = $temp_uid;
 }

  /**
   * Deletes multiple ipaddrs by ID.
   *
   * @param $ipids
   * An array of ipaddr IDs to delete.
   * @return
   * TRUE on success, FALSE otherwise.
   */
  public function delete($ipids) {
    if (!empty($ipids)) {
      $ipaddrs = $this->load($ipids, array());

      $transaction = db_transaction();

      try {
        db_delete('ipaddr')
          ->condition('ipid', $ipids, 'IN')
          ->execute();

        db_delete('ipaddr_revision')
          ->condition('ipid', $ipids, 'IN')
          ->execute();

        foreach ($ipaddrs as $ipaddr_id => $ipaddr) {
          field_attach_delete('ipaddr', $ipaddr);
        }

        // Ignore slave server temporarily to give time for the
        // saved ipaddr to be propagated to the slave.
        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('ipaddr', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $ipaddr, 'ipaddr');

      // Clear the page and block and ipaddr caches.
      cache_clear_all();
      $this->resetCache();
    }

    return TRUE;
  }

}
